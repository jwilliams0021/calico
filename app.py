# library modules
import flask
# local modules
import config

app = flask.Flask(config.default['name'], instance_relative_config=True)
app.config.from_mapping(
    SECRET_KEY=config.default['secret_key'],
    DATABASE=config.default['db_path'],
)


@app.route('/')
def home():
    return "Hello world!"


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app.run('localhost', '8080', True)

