# library modules
import os

default = {
    'name': 'Calico',
    'version': '0.1a',
    'secret_key': 'dev0.1a:10-06/2020',
    'db_path': os.path.join(os.path.expanduser('~/calico/'), 'db.sqlite')
}
